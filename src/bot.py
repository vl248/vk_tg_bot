import asyncio
import aiovk
import youtube_dl
import uuid

from aiogram import Bot as _Bot
from pathlib import Path


class Bot(_Bot):

    def __init__(self, *args, **kwargs):
        super().__init__(
            *args,
            token=self.config.bot_token,
            loop=asyncio.get_event_loop(),
            **kwargs
        )
        self.download_path = Path(__file__).parent.parent / "download"

    @property
    def vk_api(self):
        session = aiovk.TokenSession(next(self.config.vk_tokens))
        return aiovk.API(session)

    async def _download_media_task(self, options, url):
        try:
            with youtube_dl.YoutubeDL(options) as ydl:
                ydl.download([url])

            return options["outtmpl"]
        except Exception:
            return None

    async def download_media_attachment(self, url):
        file_path = self.download_path / (str(uuid.uuid1()) + '.mp4' if 'video' in url else '.jpg')

        options = {
            "outtmpl": str(file_path.resolve())
        }

        if not file_path.exists():
            return await self.loop.create_task(self._download_media_task(options, url))

        return options["outtmpl"]

    @property
    def config(self):
        return __import__("config")

    @property
    def phrases(self):
        return __import__("phrases")
