import logging
import re

from aiogram import Dispatcher, executor, types
from aiogram.types import InputFile, InputMediaPhoto, InputMediaVideo
from aiogram.utils.parts import MAX_MESSAGE_LENGTH

from utils import checks, constants
from bot import Bot


logging.basicConfig(
    filename="logs.log",
    level=logging.ERROR
)

bot = Bot()
dispatcher = Dispatcher(bot, loop=bot.loop)

post_pattern = re.compile("[.]*([0-9-]+)_([0-9-]+)[.]*")


@dispatcher.message_handler(lambda m: m.from_user, checks.can_use)
async def message_handler(message: types.Message):
    post_data = post_pattern.search(message.text)

    if post_data is None:
        return await message.reply(bot.phrases.no_url_provided)

    owner_id, post_id = post_data.groups()
    posts = await bot.vk_api.wall.getById(posts=f"{owner_id}_{post_id}")

    if len(posts) == 0:
        return await message.reply(bot.phrases.no_posts_found)

    post = posts[0]
    photos = []
    videos = []
    files = []

    for attachment in post["attachments"]:
        if "photo" in attachment:
            photos.append(attachment["photo"]["photo_604"])
        elif "video" in attachment:
            video_url = constants.VK_VIDEO_URL.format(
                owner_id=attachment["video"]["owner_id"],
                video_id=attachment["video"]["id"]
            )
            video = await bot.download_media_attachment(video_url)

            if video is not None:
                videos.append(video)

    if post["text"]:
        post["text"] = post["text"][:MAX_MESSAGE_LENGTH]

    for photo_url in photos:
        files.append(InputMediaPhoto(InputFile.from_url(photo_url)))

    for video_filepath in videos:
        files.append(InputMediaVideo(InputFile(video_filepath)))

    if len(files):
        post["text"] = post["text"][:constants.MAX_CAPTION_LEN]

    if len(files) >= 2:
        files[0].caption = post["text"]

        return await bot.send_media_group(
            bot.config.send_channel,
            files[:10]
        )

    if len(photos) > 0:
        await bot.send_photo(
            bot.config.send_channel,
            photos[0],
            caption=post["text"]
        )
    elif len(videos) > 0:
        await bot.send_video(
            bot.config.send_channel,
            videos[0],
            caption=post["text"]
        )
    elif post["text"]:
        await bot.send_message(
            bot.config.send_channel,
            post["text"]
        )


executor.start_polling(
    dispatcher,
    skip_updates=False,
    loop=bot.loop
)