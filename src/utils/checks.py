from aiogram import types


def can_use(message: types.Message):
    return message.from_user.username.lower() in message.bot.config.allowed_users

