from itertools import cycle

# Токен бота из @BotFather
bot_token: str = ""

# Сервисные ключи доступа VK API
# в "" через ,
vk_tokens: list = cycle([
    "",

])

# Список username'ов людей, которые могут пользоваться ботом
# в "" через ,
allowed_users: list = [
    "",

]

# Канал, в который должны пересылаться сообщения из вк
# с @ в начале
send_channel: str = ""
